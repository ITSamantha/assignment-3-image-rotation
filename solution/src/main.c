#include "../include/file.h"

#include "../include/image.h"

#include "../include/rotate.h"

#include "../include/bmp.h"


int main(int argc, char* argv[])
{

    if (argc != 3)
    {
        fprintf(stderr, "ERROR. Enter input bmp_file and output bmp_file.\n");
        return -1;
    }

    FILE* input_file;

    enum OpenStatus open_status = openFile(argv[1], &input_file, "r");

    if (open_status != OPEN_OKAY)
    {
        fprintf(stderr, "ERROR. Error occurred while opening file.\n");
        return -1;
    }

    struct image image = { 0 };

    enum read_status read_status = from_bmp(input_file, &image);
    enum CloseStatus close_status = closeFile(input_file);

    if (read_status != READ_OK)
    {
        fprintf(stderr, "ERROR. Error while reading bmp_file.\n");
        deleteImage(&image);
        if (close_status != CLOSE_OKAY) {
            fprintf(stderr, "ERROR. Error while closing file.\n");
        }
        return -1;
    }

    if (close_status != CLOSE_OKAY)
    {
        fprintf(stderr, "ERROR. Error while closing file.\n");
    }

    struct image result_image = rotateImage(image);

    if (result_image.height == 0 && result_image.width == 0) {
        fprintf(stderr, "ERROR. Error while rotating file.\n");
        deleteImage(&image);
        return -1;
    }

    deleteImage(&image);
    FILE* output_file;

    open_status = openFile(argv[2], &output_file, "w");

    if (open_status != OPEN_OKAY)
    {
        fprintf(stderr, "ERROR. Error while opening output file.\n");
        deleteImage(&result_image);
        return -1;
    }

    enum write_status write_status = to_bmp(output_file, &result_image);
    close_status = closeFile(output_file);

    deleteImage(&result_image);

    if (write_status != WRITE_OK)
    {
        fprintf(stderr, "ERROR. Error while writing to bmp.\n");
        if (close_status != CLOSE_OKAY) {
            fprintf(stderr, "ERROR. Error while closing file.\n");
        }
        return -1;
    }

    if (close_status != CLOSE_OKAY)
    {
        fprintf(stderr, "ERROR. Error while closing file.\n");
    }

    
    return 0;
}
