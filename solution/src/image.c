#include "../include/image.h"

struct image createImage(uint64_t img_width, uint64_t img_height)
{
    struct pixel* pixels = (struct pixel*)malloc(sizeof(struct pixel) * img_width * img_height);
    
    if (pixels == NULL) 
    {
        img_height = 0;
        img_width = 0;
    }

    return (struct image) {
        .height = img_height,
            .width = img_width,
            .data = pixels
    };
}

void deleteImage(struct image* image)
{
    free(image->data);
    image->data = NULL;
}

struct pixel* getPixel(uint64_t x, uint64_t y, struct image image)
{
    if (x < image.width && y < image.height && image.data != NULL)
    {
        return &image.data[x + image.width * y];
    }

    return NULL;
}

void setPixel(uint64_t x, uint64_t y, struct pixel *pixels, struct image image)
{
    if (x < image.width && y < image.height && image.data != NULL && pixels!= NULL)
    {
        image.data[x + image.width * y] = *pixels;
    }
}
