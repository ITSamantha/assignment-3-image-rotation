#include "../include/file.h"

#include <stdio.h>

enum OpenStatus openFile(char* file_name, FILE** file, char* file_mode) {

    *file = fopen(file_name, file_mode);

    if (file == NULL) {
        return OPEN_ERROR_OCCURRED;
    }

    return OPEN_OKAY;

}

enum CloseStatus closeFile(FILE* file_name) {
    if (fclose(file_name) == EOF) {
        return CLOSE_ERROR_OCCURRED;
    }
    return CLOSE_OKAY;
}
