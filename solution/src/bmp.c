#include "../include/bmp.h"

#define PADDING_NUMBER 4

inline static uint8_t paddingSize(const uint64_t img_width)
{

    return (PADDING_NUMBER - (sizeof(struct pixel) * img_width)) % PADDING_NUMBER;
}

inline static size_t fileSize(const struct image* image)
{
    return (image->width * sizeof(struct pixel) + paddingSize(image->width)) * image->height;
}

inline static struct bmp_header createHeader(const struct image* image)
{
    return (struct bmp_header) {
        .bfType = IMAGE_TYPE,
            .bfileSize = fileSize(image),
            .bfReserved = ZERO_VALUE,
            .bOffBits = OFF_BITS,
            .biSize = IMAGE_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = NUMBER_OF_PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = ZERO_VALUE,
            .biSizeImage = fileSize(image) + sizeof(struct bmp_header),
            .biXPelsPerMeter = ZERO_VALUE,
            .biYPelsPerMeter = ZERO_VALUE,
            .biClrUsed = ZERO_VALUE,
            .biClrImportant = ZERO_VALUE
    };
}

inline static enum read_status getHeader(FILE* file, struct bmp_header* header)
{
    if (!fread(header, sizeof(struct bmp_header), 1, file))
    {
        return READ_INVALID_HEADER;
    }

    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* image)
{

    struct bmp_header header = { 0 };
    if (!(getHeader(in, &header) == READ_OK))
    {
        return READ_INVALID_HEADER;
    }

    *image = createImage(header.biWidth, header.biHeight);

    if (image->height == 0 && image->width == 0) {
        return READ_INVALID_BITS;
    }

    for (size_t i = 0; i < image->height; ++i)
    {
        if (!fread(image->data + i * image->width, sizeof(struct pixel), image->width, in))
        {
            deleteImage(image);
            return READ_INVALID_BITS;
        }
        if (fseek(in, paddingSize(image->width), SEEK_CUR))
        {
            deleteImage(image);
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* image)
{
    if (image->data == NULL)
    {
        return WRITE_ERROR;
    }

    struct bmp_header header = createHeader(image);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
    {
        return WRITE_ERROR;
    }

    if (fseek(out, header.bOffBits, SEEK_SET))
    {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < image->height; ++i)
    {

        if (fwrite(image->data + i * image->width, sizeof(struct pixel), image->width, out) != image->width || fseek(out, paddingSize(image->width), SEEK_CUR) == -1)
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
