#include "../include/rotate.h"
#include <stdlib.h>

struct image rotateImage(struct image source_image)
{

    struct pixel* pixels = (struct pixel*)malloc(sizeof(struct pixel) * source_image.width * source_image.height);

    if (pixels == NULL) 
    {
        return (struct image) {0};
    }
    
    struct image rotated_image = constructImage(source_image.height, source_image.width, pixels);

    for (size_t y = 0; y < source_image.height; ++y)
    {
        for (size_t x = 0; x < source_image.width; ++x)
        {
            struct pixel *current_pixel = getPixel(x, y, source_image);

            if (current_pixel == NULL)
            {
                free(pixels);
                return (struct image) { 0 };
            }

            setPixel(rotated_image.width - 1 - y, x, current_pixel , rotated_image);
        }
    }
    return rotated_image;
}
