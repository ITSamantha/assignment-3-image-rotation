#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


struct __attribute__((packed)) pixel
{
    uint8_t b, g, r;
};

struct image
{
    uint64_t width, height;
    struct pixel* data;
};

struct image createImage(uint64_t img_width, uint64_t img_height);

void deleteImage(struct image* image);

static inline struct image constructImage(uint64_t img_width, uint64_t img_height, struct pixel* pixel_data)
{
    return (struct image) { .width = img_width, .height = img_height, .data = pixel_data };
}

struct pixel* getPixel(uint64_t x, uint64_t y, struct image image);

void setPixel(uint64_t x, uint64_t y, struct pixel *pixels, struct image image);

#endif
