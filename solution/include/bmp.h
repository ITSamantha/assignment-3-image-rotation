#ifndef BMP_H
#define BMP_H

#include "./image.h"

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};


/*  serializer   */
enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum ImageConstants {
    IMAGE_TYPE = 19778,
    ZERO_VALUE = 0,
    OFF_BITS = 54,
    IMAGE_SIZE = 40,
    NUMBER_OF_PLANES = 1,
    BIT_COUNT = 24
};

enum read_status from_bmp(FILE* in, struct image* image);

enum write_status to_bmp(FILE* out, const struct image* image);

#endif
