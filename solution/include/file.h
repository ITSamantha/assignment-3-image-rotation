#ifndef FILE_H
#define FILE_H

#include "image.h"

#include <stdbool.h>
#include <stdio.h>

enum  OpenStatus {
    OPEN_OKAY,
    OPEN_ERROR_OCCURRED
};

enum OpenStatus openFile(char* file_name, FILE** file, char* file_mode);

enum CloseStatus {
    CLOSE_OKAY,
    CLOSE_ERROR_OCCURRED
};

enum CloseStatus closeFile(FILE* file_name);

#endif
