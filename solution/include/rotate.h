#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"


struct image rotateImage(struct image source_image);

#endif
